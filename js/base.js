function truncateText(item, max_words) {
  var truncateElement = item;
  var originalText = truncateElement.textContent;
  var maxWords = max_words; // Set your desired number of words

  if (originalText.split(" ").length > maxWords) {
    var truncatedText = originalText.split(" ").slice(0, maxWords).join(" ");
    truncateElement.textContent = truncatedText + "...";
    //truncateElement.innerHtml = "<p>"+truncatedText + "..."+"</p>";
    //truncateElement.classList.add("truncated");
  }
}
