document.addEventListener("DOMContentLoaded", function() {
    // Code to be executed when the DOM is ready
    console.log("DOM is ready!");
    Array.prototype.forEach.call(document.getElementsByClassName("desta-new-item-lnk"), function(el) {
        truncateText(el, 5);
    });

    Array.prototype.forEach.call(document.getElementsByClassName("desta-new-item-lnk"), function(el) {
        truncateText(el, 5);
    });

    var multimediaModal = new bootstrap.Modal(document.getElementById("multimediaModal"), {});

    document.getElementById("multimediaModal").addEventListener('hidden.bs.modal', event => {
      document.querySelector('#multimediaModal .modal-body').innerHTML = "";
    })

    var multimedias = document.querySelectorAll('.open-multimedia');

    Array.from(multimedias).forEach(multimedia =>{
        multimedia.addEventListener('click', function(event){
            document.querySelector('#multimediaModal #multimediaModalLabel').textContent = this.textContent;
            document.querySelector('#multimediaModal .modal-body').innerHTML = '<iframe width="468" height="315" src="https://www.youtube.com/embed/'+this.getAttribute('data-yt')+'" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>';
            multimediaModal.show();
        });
    })
    
});